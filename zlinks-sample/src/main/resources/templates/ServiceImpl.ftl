<#import "base/date.ftl" as dt>

<#assign beanName = table.beanName/>
<#assign beanNameUncap_first = beanName?uncap_first/>

import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * ${table.tableDesc} Service实现
 * @Author:   zhangke
 * @Date:    ${.now}
 */
@Service
public class ${table.beanName}ServiceImpl implements ${table.beanName}Service {

  @Autowired
  private ${table.beanName}Dao ${table.beanName?uncap_first}Dao;

  /**
   *条件查询 ${table.tableDesc}
   * @param record 封装查询条件 
   */
  public List<${table.beanName}> selectByCondition(${table.beanName} record) {
    return ${table.beanName?uncap_first}Dao.selectByCondition(record);
  }

  /**
   *更新 ${table.tableDesc}
   * @param record 要更新的 ${table.tableDesc}
   */
  public int updateByPrimaryKeySelective(${table.beanName} record) {
    return ${table.beanName?uncap_first}Dao.updateByPrimaryKeySelective(record);
  }
  
  /**
   *根据主键删除 ${table.tableDesc}
   * @param ${table.beanName?uncap_first}Id 要删除的 ${table.tableDesc}id
   */
  public int deleteByPrimaryKey(Long ${table.beanName?uncap_first}Id) {
    return ${table.beanName?uncap_first}Dao.deleteByPrimaryKey(${table.beanName?uncap_first}Id);
  }
  
  /**
   *新增 ${table.tableDesc}
   * @param record 要新增的${table.tableDesc}
   */
  public int insertSelective(${table.beanName} record) {
    //查询是否已经有重复记录
    //if (${table.beanName?uncap_first}Dao.selectRepeatRecord(record) != null) {
    //  throw new MESException("该记录已存在");
    //}
    return ${table.beanName?uncap_first}Dao.insertSelective(record);
  }
  
  /**
   *根据主键查询${table.tableDesc}
   * @param ${table.beanName?uncap_first}Id 要查询的 ${table.tableDesc}id
   */
  public ${table.beanName} selectByPrimaryKey(Long ${table.beanName?uncap_first}Id) {
    return ${table.beanName?uncap_first}Dao.selectByPrimaryKey(${table.beanName?uncap_first}Id);
  }

}