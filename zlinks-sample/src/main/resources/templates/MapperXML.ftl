<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
	PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
	"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<#assign beanName = table.beanName/>
<#assign tableName = table.tableName/>
<#macro mapperEl value>${r"#{"}${value.javaName},jdbcType=${value.jdbcType}}</#macro>
<#macro recordEl value>${r"#{"}record.${value.javaName},jdbcType=${value.jdbcType}}</#macro>

<#--<#macro batchMapperEl value>${r"#{"}${value}}</#batchMapperEl<#if table.prefix!="">
<#assign bean = conf.entityPackage+"."+table.prefix+"."+beanName/>
<#assign mapper = conf.daoPackage+"."+table.prefix+"."+beanName+"Mapper"/>
<#else>
<#assign bean = conf.entityPackage+"."+beanName/>
<#assign mapper = conf.daoPackage+"."+beanName+"Mapper"/>
</#if>
<#assign propertiesAnColumns = table.propertiesAnColumns/>
<#assign keys = propertiesAnColumns?keys/>
<#assign primaryKey = table.primaryKey/>
<#assign keys2 = primaryKey?keys/>
<#assign insertPropertiesAnColumns = table.insertPropertiesAnColumns/>
<#assign keys3 = insertPropertiesAnColumns?keys/>>-->

<mapper namespace="----命名空间----">

	<sql id="basicSelectSql">
		<#list table.columns as prop>
		${prop.colName} AS ${prop.javaName}<#if prop_has_next>,</#if>
		</#list>
	</sql>
	
<!--非模糊查询
	<sql id="basicWhereColumn">
		<#list table.columns as prop>
			<if test="${prop.javaName} != null">
				AND ${prop.colName} = <@mapperEl prop/>
			</if>
		</#list>
	</sql>
-->
<!--String默认模糊查询--> 
	<sql id="basicWhereColumn">
		<#list table.columns as prop>
		<#if prop.javaType == "String">
		  <if test="${prop.javaName} != null">
        AND ${prop.colName} like concat('%',concat(<@mapperEl prop/>,'%'))
      </if>
		</#if>
		<#if prop.javaType != "String">
			<if test="${prop.javaName} != null">
				AND ${prop.colName} = <@mapperEl prop/>
			</if>
		</#if>
		</#list>
	</sql>

<!--String默认模糊查询,查询参数有多个对象的情况如(List list,User record)--> 
	<sql id="basicRecordWhereColumn">
		<#list table.columns as prop>
		<#if prop.javaType == "String">
		  <if test="record.${prop.javaName} != null">
        AND ${prop.colName} like concat('%',concat(<@recordEl prop/>,'%'))
      </if>
		</#if>
		<#if prop.javaType != "String">
			<if test="record.${prop.javaName} != null">
				AND ${prop.colName} = <@recordEl prop/>
			</if>
		</#if>
		</#list>
	</sql>
	
	<sql id="basicWhereEntitySql">
		<where>
		<include refid="basicWhereColumn"/>
		</where>
	</sql>
	
	<sql id="basicWhereMapSql">
		<where>
		<include refid="basicWhereColumn"/>
		</where>
	</sql>

  <update id="updateByPrimaryKeySelective" parameterType="--类名--">
    UPDATE ${tableName}
    <set>
    <#list table.columns as prop>
      <if test="${prop.javaName} != null">
         ${prop.colName} = <@mapperEl prop/>,
      </if>
    </#list>
    </set>
    <where>
      ${table.columns[0].colName} = <@mapperEl table.columns[0]/>
    </where>
  </update>
  
  <delete id="deleteByPrimaryKey" parameterType="java.lang.Long">
    delete from ${tableName}
    <where>
       ${table.columns[0].colName} = <@mapperEl table.columns[0]/>
    </where>
  </delete>

  <insert id="insertSelective"  parameterType="--类名--">
    INSERT INTO 
    ${tableName}
    <trim prefix="(" suffix=")" suffixOverrides=",">
      <#list table.columns as prop>
      <if test="${prop.javaName} != null">
         ${prop.colName},
      </if>
      </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
          <#list table.columns as prop>
      <if test="${prop.javaName} != null">
         <@mapperEl prop/>,
      </if>
    </#list>
    </trim>
  </insert>
  
  <insert id="insertSelectiveReuturnId" useGeneratedKeys="true" keyProperty="${table.columns[0].javaName}"  parameterType="--类名--">
    INSERT INTO 
    ${tableName}
    <trim prefix="(" suffix=")" suffixOverrides=",">
      <#list table.columns as prop>
      <if test="${prop.javaName} != null">
         ${prop.colName},
      </if>
      </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
          <#list table.columns as prop>
      <if test="${prop.javaName} != null">
         <@mapperEl prop/>,
      </if>
    </#list>
    </trim>
  </insert>


  <insert id="insertBatch" parameterType="--类名--">
    INSERT INTO 
    ${tableName}
    <trim prefix="(" suffix=")" suffixOverrides=",">
      <#list table.columns as prop>
         ${prop.colName}<#if prop_has_next>,</#if>
      </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides=",">
      <foreach collection="list" item="item" index="index" separator="," >
      <#list table.columns as prop>
         <@mapperEl prop/><#if prop_has_next>,</#if>
      </#list>
      </foreach>
    </trim>
  </insert>

  <sql id="whereContentAll">
    <where>
       <include refid="basicWhereColumn"/>
    </where>
  </sql>

  <select id="selectByCondition" resultType="--类名--" >
    SELECT
      <include refid="basicSelectSql" />
    FROM ${tableName}
      <include refid="whereContentAll"/>
  </select>
  
  <select id="selectByPrimaryKey" resultType="--类名--" >
    SELECT
      <include refid="basicSelectSql" />
    FROM ${tableName}
      <where>   
        ${table.columns[0].colName} = <@mapperEl table.columns[0]/>
      </where>
  </select>
  
	<select id="selectRepeatRecord" resultType="--类名--" >
    SELECT
      <include refid="basicSelectSql" />
    FROM ${tableName}
      <include refid="whereContentAll"/>
    LIMIT 1
  </select>
  

      

      <#list table.idCols as col>
   <select id="selectBy${col.javaName?cap_first}List" resultType="--类名--" >
     SELECT
      <include refid="basicSelectSql" />
     FROM ${tableName}
      <where>
      <if test = "idList != null">
        ${col.colName} in(
        <foreach collection="idList" item="id" index="index" separator="," >
          ${r"#{"}id}
        </foreach>
        )
      </if>
      <if test = "record != null">
        <include refid="basicRecordWhereColumn"/>
      </if>
      </where>
    </select>
      </#list>
      
    <#list table.idCols as outcol>
      <#list table.idCols as col>
      <#if col.colName != outcol.colName>
   <select id="select${outcol.javaName?cap_first}ListBy${col.javaName?cap_first}List" resultType="java.lang.Long" >
     SELECT
        ${outcol.colName}
     FROM ${tableName}
      <where>
      <if test = "idList != null">
        ${col.colName} in(
        <foreach collection="idList" item="id" index="index" separator="," >
          ${r"#{"}id}
        </foreach>
        )
      </if>
      <if test = "record != null">
        <include refid="basicRecordWhereColumn"/>
      </if>
      </where>
    </select>
    </#if>
      </#list>
    </#list>
</mapper>