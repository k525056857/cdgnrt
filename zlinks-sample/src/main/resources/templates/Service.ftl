

import java.util.List;
/**
 * ${table.tableDesc} Service
 * @Author:   zhangke
 * @Date:    ${.now}
 */
public interface ${table.beanName}Service {

  /**
   *条件查询 ${table.tableDesc}
   * @param record 封装查询条件 
   */
  List<${table.beanName}> selectByCondition(${table.beanName} record);

  /**
   *更新  ${table.tableDesc}
   * @param record 要更新的 ${table.tableDesc}
   */
  int updateByPrimaryKeySelective(${table.beanName} record);
  
  /**
   *根据主键删除  ${table.tableDesc}
   * @param ${table.columns[0].javaType} 要删除的  ${table.tableDesc}id
   */
  int deleteByPrimaryKey(${table.columns[0].javaType} ${table.columns[0].javaName});
  
  /**
   *新增  ${table.tableDesc}
   * @param record 要新增的 ${table.tableDesc}
   */
  int insertSelective(${table.beanName} record);
  
  /**
   *根据主键查询 ${table.tableDesc}
   * @param ${table.columns[0].javaType} 要查询的  ${table.tableDesc}id
   */
  ${table.beanName} selectByPrimaryKey(${table.columns[0].javaType} ${table.columns[0].javaName});
  
}