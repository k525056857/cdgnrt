
<#assign beanName = table.beanName/>
<#assign beanNameuncap_first = beanName?uncap_first/>
<#assign implName = beanNameuncap_first+"ServiceImpl"/>
<#assign serviceName = beanNameuncap_first+"Service"/>

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybertron.modules.piece.web.${table.beanName}Service;
import com.cybertron.service.mes.entity.${table.beanName};
import com.cybertron.service.mes.exception.MESException;
import com.cybertron.utils.PageBean;
import com.cybertron.utils.ResponseMessageUtils;
import com.cybertron.utils.ResultMsg;
import com.github.pagehelper.PageHelper;

import io.swagger.annotations.ApiOperation;

/**
 * ${table.tableDesc} controller
 * Author:   zhangke
 * Date:     2019-1-28 10:21:07
 */
@RestController
@RequestMapping("${table.beanName?uncap_first}")
public class ${table.beanName}Controller {

  @Autowired
  private ${table.beanName}Service ${table.beanName?uncap_first}Service;
  
  /**
   * 添加${table.tableDesc}
   * @param record 要添加的${table.tableDesc}
   * @return
   */
  @PostMapping("add/${beanNameuncap_first}")
  @ApiOperation(value = "add/${beanNameuncap_first}", httpMethod = "POST", response = ResultMsg.class, notes = "添加${table.tableDesc}")
  public ResultMsg add${table.beanName}(@RequestBody ${table.beanName} record) {
    //校验前端传来的参数
    //checkInsertParams(record);
    int rows = ${table.beanName?uncap_first}Service.insertSelective(record);
    if (rows == 1) {
      return ResponseMessageUtils.success("添加成功");
    } else {
      return ResponseMessageUtils.error("添加失败");
    }
  }

  /**
   * 获取${table.tableDesc} 
   * @param pageNum  页号
   * @param pageSize  页面大小
   <#list table.columns as prop>
   * @param${prop.javaType} ${prop.javaName?cap_first} ${prop.remark} 
   </#list>
   * @return
   */
  @GetMapping("get/${beanNameuncap_first}")
  @ApiOperation(value = "get/${beanNameuncap_first}", httpMethod = "GET", response = ResultMsg.class, notes = "获取${table.tableDesc}")
  public ResultMsg get${table.beanName}(@RequestParam Integer pageNum, @RequestParam Integer pageSize, 
      <#list table.columns as prop>
      @RequestParam(value = "${prop.javaName?cap_first}", required = false) ${prop.javaType} ${prop.javaName?uncap_first}<#if prop_has_next>,</#if>
      </#list>
      ) { 
    ${table.beanName} record = new ${table.beanName}();
    <#list table.columns as prop>
    record.set${prop.javaName?cap_first}(${prop.javaName});
    </#list>
    PageHelper.startPage(pageNum, pageSize);
    List<${table.beanName}> resultList = ${table.beanName?uncap_first}Service.selectByCondition(record);
    PageBean<${table.beanName}> pageBean = new PageBean<>(resultList);
    return ResponseMessageUtils.success("查询成功", pageBean);
  }

  /**
   * 删除${table.tableDesc}
   * @param ${table.beanName?uncap_first}Id 要删除的${table.tableDesc}主键
   * @return
   */
  @DeleteMapping("delete/${beanNameuncap_first}")
  @ApiOperation(value = "delete/${beanNameuncap_first}", httpMethod = "DELETE", response = ResultMsg.class, notes = "删除${table.tableDesc} ")
  public ResultMsg delete${table.beanName}(@RequestParam Long ${table.beanName?uncap_first}Id) {
    int rows = ${table.beanName?uncap_first}Service.deleteByPrimaryKey(${table.beanName?uncap_first}Id);
    if (rows == 1) {
      return ResponseMessageUtils.success("删除成功");
    } else {
      return ResponseMessageUtils.error("删除失败");
    }
  }

  /**
   * 更新${table.tableDesc}
   * @param record 要更新的${table.tableDesc}
   * @return
   */
  @PatchMapping("update/${beanNameuncap_first}")
  @ApiOperation(value = "update/${beanNameuncap_first}", httpMethod = "PATCH", response = ResultMsg.class, notes = "更新${table.tableDesc}")
  public ResultMsg update${table.beanName}(@RequestBody ${table.beanName} record) {
    //校验前端传来的参数
    //checkUpdateParams(record);
    int rows = ${table.beanName?uncap_first}Service.updateByPrimaryKeySelective(record);
    if (rows == 1) {
      return ResponseMessageUtils.success("更新成功");
    } else {
      return ResponseMessageUtils.error("更新失败");
    }
  }
  
  /**
   * 检验要插入的参数是否合法
   * @param record 要检验的${table.tableDesc}
   */
  private void checkInsertParams(${table.beanName} record) {
    if (record == null) {
      throw new MESException("参数完全为空");
    }
    <#list table.columns as prop>
    <#if prop.javaType == "String">
    if (record.get${prop.javaName?cap_first}() == null || StringUtils.isBlank(record.get${prop.javaName?cap_first}())) {
      throw new MESException("参数${prop.javaName}不能为空");
    }
    </#if>
    <#if prop.javaType != "String">
    if (record.get${prop.javaName?cap_first}() == null) {
      throw new MESException("参数${prop.javaName} 不能为空");
    }
    </#if>
    </#list>
  }
  
  /**
   * 检验要更新的参数是否合法
   * @param record 要检验的${table.tableDesc}
   */
  private void checkUpdateParams(${table.beanName} record) {
    if (record == null) {
      throw new MESException("参数完全为空");
    }
    <#list table.columns as prop>
    <#if prop.javaType == "String">
    if (StringUtils.isBlank(record.get${prop.javaName?cap_first}())) {
      throw new MESException("参数${prop.javaName}不能为空");
    }
    </#if>
    <#if prop.javaType != "String">
    if (record.get${prop.javaName?cap_first}() == null) {
      throw new MESException("参数${prop.javaName} 不能为空");
    }
    </#if>
    </#list>
  }
}

