<#--
package ${conf.daoPackage}<#if table.prefix!="">.${table.prefix}</#if>;
-->

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
/**
 * ${table.tableDesc} DAO
 * @Author:   zhangke
 * @Date:    ${.now}
 */
@Mapper
public interface ${table.beanName}Dao {

  /**
   *条件查询 ${table.tableDesc}
   * @param record 封装查询条件 
   */
  List<${table.beanName}> selectByCondition(${table.beanName} record);

  /**
   *更新  ${table.tableDesc}
   * @param record 要更新的 ${table.tableDesc}
   */
  int updateByPrimaryKeySelective(${table.beanName} record);
  
  /**
   *根据主键删除  ${table.tableDesc}
   * @param ${table.columns[0].javaName} 要删除的  ${table.tableDesc}id
   */
  int deleteByPrimaryKey(${table.columns[0].javaType} ${table.columns[0].javaName});
  
  /**
   *新增  ${table.tableDesc}
   * @param record 要新增的 ${table.tableDesc}
   */
  int insertSelective(${table.beanName} record);
  
  /**
   *根据主键查询 ${table.tableDesc}
   * @param ${table.columns[0].javaName} 要查询的  ${table.tableDesc}id
   */
  ${table.beanName} selectByPrimaryKey(${table.columns[0].javaType} ${table.columns[0].javaName});
  
  /**
   *查询重复的 ${table.tableDesc}
   * @param record 要查重的 ${table.tableDesc}
   */
  ${table.beanName} selectRepeatRecord(${table.beanName} record);
  
   <#list table.idCols as outcol>
      <#list table.idCols as col>
      <#if col.colName != outcol.colName>
  
  /**
   *根据${col.remark}列表和record封装的条件,查询${outcol.remark}列表
   */
  List<Long> select${outcol.javaName?cap_first}ListBy${col.javaName?cap_first}List(List<Long> idList,${table.beanName} record);
       </#if>
       </#list>
    </#list>    
     <#list table.idCols as col>
  
  /**
   *根据${col.remark}列表和record封装的条件,查询${table.tableDesc}列表
   */
  List<${table.beanName}> selectBy${col.javaName?cap_first}List(List<Long> idList,${table.beanName} record);
      </#list>  
    
  
}