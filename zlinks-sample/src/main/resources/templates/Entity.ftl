
<#-- package ${conf.entityPackage}<#if table.prefix!="">.${table.prefix}</#if>; -->

<#list table.propTypePackages as package>
${package}
</#list>
/**
* 表名：${table.tableName},描述：${table.tableDesc}
* @Author:   zhangke
* @Date:    ${.now}
*/
public class ${table.beanName} {
  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 1L;
  <#list table.columns as prop>
  /**
   *${prop.remark}
   */
  private ${prop.javaType} ${prop.javaName};
  </#list>
  
  <#list table.columns as prop>
  
  public ${prop.javaType} get${prop.javaName?cap_first}() {
    return this.${prop.javaName};
  }
  
  public void set${prop.javaName?cap_first}(${prop.javaType} ${prop.javaName}) {
    this.${prop.javaName} = ${prop.javaName};
  }
  </#list>

}

