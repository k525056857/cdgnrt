package com.zlinks.samples.bean;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 表的一列对应的所有信息
 * @author zhangke
 * @date 2019年1月29日 上午11:52:38
 */
public class ColumnInfo implements Serializable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 列名
	 */
	private String colName;
	/**
	 * 列数据类型
	 */
	private String colType;
	/**
	 * 字段注释
	 */
	private String remark;
 /**
  * java属性名
  */
 private String javaName;
 /**
  * java属性类型
  */
 private String javaType;
 
 /**
  * jdbc类型
  */
 private String jdbcType;
 
 

	public String getcolName() {
		return colName;
	}

	public void setcolName(String colName) {
		this.colName = colName;
	}

	public String getcolType() {
		return colType;
	}

	public void setcolType(String colType) {
		this.colType = colType;
	}

	public String getremark() {
		return remark;
	}

	public void setremark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

  public String getjavaName() {
    return javaName;
  }

  public void setjavaName(String javaName) {
    this.javaName = javaName;
  }

  public String getjavaType() {
    return javaType;
  }

  public void setjavaType(String javaType) {
    this.javaType = javaType;
  }

  public String getJdbcType() {
    return jdbcType;
  }

  public void setJdbcType(int jdbcCode) {
    switch (jdbcCode) {
    case (-7):{
      this.jdbcType="BIT";
      break;
    }
    case (-6):{
      this.jdbcType="TINYINT";
      break;
    }
    case (5):{
      this.jdbcType="SMALLINT";
      break;
    }
    case (4):{
      this.jdbcType="INTEGER";
      break;
    }
    case (-5):{
      this.jdbcType="BIGINT";
      break;
    }
    case (6):{
      this.jdbcType="FLOAT";
      break;
    }
    case (7):{
      this.jdbcType="REAL";
      break;
    }
    case (8):{
      this.jdbcType="DOUBLE";
      break;
    }
    case (2):{
      this.jdbcType="NUMERIC";
      break;
    }
    case (3):{
      this.jdbcType="DECIMAL";
      break;
    }
    case (1):{
      this.jdbcType="CHAR";
      break;
    }
    case (12):{
      this.jdbcType="VARCHAR";
      break;
    }
    case (-1):{
      this.jdbcType="LONGVARCHAR";
      break;
    }
    case (91):{
      this.jdbcType="DATE";
      break;
    }
    case (92):{
      this.jdbcType="TIME";
      break;
    }
    case (93):{
      this.jdbcType="TIMESTAMP";
      break;
    }
    case (-2):{
      this.jdbcType="BINARY";
      break;
    }
    case (-3):{
      this.jdbcType="VARBINARY";
      break;
    }
    case (-4):{
      this.jdbcType="LONGVARBINARY";
      break;
    }
    case (0):{
      this.jdbcType="NULL";
      break;
    }
    case (1111):{
      this.jdbcType="OTHER";
      break;
    }
    case (2000):{
      this.jdbcType="JAVA_OBJECT";
      break;
    }
    case (2001):{
      this.jdbcType="DISTINCT";
      break;
    }
    case (2002):{
      this.jdbcType="STRUCT";
      break;
    }
    case (2003):{
      this.jdbcType="ARRAY";
      break;
    }
    case (2004):{
      this.jdbcType="BLOB";
      break;
    }
    case (2005):{
      this.jdbcType="CLOB";
      break;
    }
    case (2006):{
      this.jdbcType="REF";
      break;
    }
    case (70):{
      this.jdbcType="DATALINK";
      break;
    }
    case (16):{
      this.jdbcType="BOOLEAN";
      break;
    }
    case (-8):{
      this.jdbcType="ROWID";
      break;
    }
    case (-15):{
      this.jdbcType="NCHAR";
      break;
    }
    case (-9):{
      this.jdbcType="NVARCHAR";
      break;
    }
    case (-16):{
      this.jdbcType="LONGNVARCHAR";
      break;
    }
    case (2011):{
      this.jdbcType="2011";
      break;
    }
    case (2009):{
      this.jdbcType="SQLXML";
      break;
    }
    case (2012):{
      this.jdbcType="REF_CURSOR";
      break;
    }
    case (2013):{
      this.jdbcType="TIME_WITH_TIMEZONE";
      break;
    }
    case (2014):{
      this.jdbcType="TIMESTAMP_WITH_TIMEZONE";
      break;
    }
      

    }
  }
}
